package com.example.srpingwebj11.controllers;


import com.example.srpingwebj11.Srpingwebj11Application;
import com.example.srpingwebj11.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductController {

    static final String ApiBaseUrl = "/srpingwebj11";

    @GetMapping(ApiBaseUrl + "/products")
    public ArrayList<ProductModel> getProducts() {
        System.out.println("getProducts");

        return Srpingwebj11Application.productModels;
    }

    @GetMapping(ApiBaseUrl + "/products/{id}")
    public ProductModel getProductById(@PathVariable String id) {
        System.out.println("getProductById");
        System.out.println("La id del producto a obtener es " + id);
        int index = 0;

        for (ProductModel product : Srpingwebj11Application.productModels) {
            if (product.getId().equals(id)) {
                System.out.println("Producto encontrado");
                index = Srpingwebj11Application.productModels.indexOf(product);
            }
        }
        return Srpingwebj11Application.productModels.get(index);
    }

    @PostMapping(ApiBaseUrl + "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct) {
        System.out.println("createProduct");
        System.out.println("La id del producto a crear es " + newProduct.getId());
        System.out.println("la descripción del producto a crear es " + newProduct.getDesc());
        System.out.println("El precio del producto a crear es " + newProduct.getPrice());

        Srpingwebj11Application.productModels.add(newProduct);
        int index = Srpingwebj11Application.productModels.size()-1;

        return Srpingwebj11Application.productModels.get(index);
    }

    @PutMapping(ApiBaseUrl + "/products/{id}")
    public ProductModel updateProduct(@RequestBody ProductModel product, @PathVariable String id) {
        System.out.println("updateProduct");
        System.out.println("La id del producto a actualizar en parametro URL es " + id);
        System.out.println("La id del producto a actualizar es: " + product.getId());
        System.out.println("La descripcion del producto a actualizar es: " + product.getDesc());
        System.out.println("El precio del producto a actualizar es: " + product.getPrice());

        for (ProductModel productInList : Srpingwebj11Application.productModels) {
            if (productInList.getId().equals(id)) {
                int index = Srpingwebj11Application.productModels.indexOf(productInList);
                System.out.println("Index a actualizar: " + index);
                productInList.setId(product.getId());
                Srpingwebj11Application.productModels.get(index).setDesc(product.getDesc());
                //productInList.setPrice(product.getPrice());
                Srpingwebj11Application.productModels.get(index).setPrice(product.getPrice());
            }
        }
        return product;
    }

    @PatchMapping(ApiBaseUrl + "/products/{id}")
    public ProductModel patchProduct(@RequestBody ProductModel productData, @PathVariable String id) {
        System.out.println("patchProduct");
        System.out.println("La id del producto a actualizar en parametro URL es " + id);
        System.out.println("La id del producto a actualizar es: " + productData.getId());
        System.out.println("La descripcion del producto a actualizar es: " + productData.getDesc());
        System.out.println("El precio del producto a actualizar es: " + productData.getPrice());

        for (ProductModel productInList : Srpingwebj11Application.productModels) {
            if (productInList.getId().equals(id)) {
                int index = Srpingwebj11Application.productModels.indexOf(productInList);
                System.out.println("Index a actualizar: " + index);
                productInList.setId(productData.getId());
                if (productData.getDesc() != null) {
                    //productInList.setDesc(product.getDesc());
                    Srpingwebj11Application.productModels.get(index).setDesc(productData.getDesc());
                }
                if (productData.getPrice() != 0) {
                    //productInList.setPrice(product.getPrice());
                    Srpingwebj11Application.productModels.get(index).setPrice(productData.getPrice());
                }
            }
        }
        return productData;
    }

    @DeleteMapping(ApiBaseUrl + "/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id) {
        System.out.println("deleteProduct");
        System.out.println("La id del producto a borrar en parametro URL es " + id);

        ProductModel productDeleted = new ProductModel();
        boolean encontrado = false;
        int index = 0;
        for (ProductModel product : Srpingwebj11Application.productModels) {
            if (product.getId().equals(id)) {
                encontrado = true;
                index = Srpingwebj11Application.productModels.indexOf(product);
                System.out.println("Index a borrar: " + index);
                System.out.println("La id del producto a actualizar es: " + product.getId());
                System.out.println("La descripcion del producto a borrar es: " + product.getDesc());
                System.out.println("El precio del producto a borrar es: " + product.getPrice());
                productDeleted = product;
            }
        }

        if (encontrado) {
            Srpingwebj11Application.productModels.remove(index);
        }

        return productDeleted;
    }
}
