package com.example.srpingwebj11.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @RequestMapping("/")
    public String index() {
        return "Hola mundo desde API Tech U!";
    }

    @RequestMapping("/hello")
    public String Hello(@RequestParam(value = "name", defaultValue = "Tech U") String name,
                        @RequestParam(value = "lastname",defaultValue = " ") String lastname) {
        return String.format("Hola %s %s",name,lastname);
    }
}
