package com.example.srpingwebj11;

import com.example.srpingwebj11.models.ProductModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

@SpringBootApplication
public class Srpingwebj11Application {

	public static ArrayList<ProductModel> productModels;

	public static void main(String[] args) {
		SpringApplication.run(Srpingwebj11Application.class, args);
		Srpingwebj11Application.productModels = Srpingwebj11Application.getTestData();

	}

	private static ArrayList<ProductModel> getTestData() {

		ArrayList<ProductModel> productModels = new ArrayList<>();

		productModels.add(
				new ProductModel(
						"1"
						,"Producto 1"
						,10
				)
		);

		productModels.add(
				new ProductModel(
						"2"
						,"Producto 2"
						,20
				)
		);

		productModels.add(
				new ProductModel(
						"3"
						,"Producto 3"
						,30
				)
		);

		productModels.add(
				new ProductModel(
						"4"
						,"Producto 4"
						,40
				)
		);

		return productModels;
	}

}
